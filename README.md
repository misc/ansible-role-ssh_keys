Ansible role to copy my ssh keys around

This role add and remove ssh keys for root, and also on various cloud
providers (with various being "digital ocean" for now)

# Using the role

The role is made to be used as dependency of another role
that hold the keys in the files directory. This way, we can separate
the logic from the data, making the logic reusable.

The keys should be in a file with the suffix .pub.

## Adding keys on a user

By default, the role operate on the root user. It will add all keys from
the file directory who end with a .pub suffix.

## Removing keys from a user

If there is keys in a directory `remove`, then the keys will be removed. 
This allow to seamlessly move keys after they have been rotated and/or compromised.

## Adding keys to Digital Ocean

If the variable `digital_ocean_token` is defined, then the ssh keys
will be uploaded to Digital Ocean. There is no support for now
to remove keys.
